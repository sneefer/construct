/* модуль конструктора, добавляет и управляет блоками
 *
 *	@autor: Chavusov Stanislav
 */
var module_add = {
	
	init: function(){
		module_add.container = $("#perview_container");
		module_add.patterns = $("#block_patterns");
		module_add.loadPlugins();
	},
	
	container: null,
	patterns: null,
	image_dialog: null,
	
	loadPlugins: function(){
		
		//add block full width init:
		$(".add_block_full_red").click(function(){
			var clone_obj = module_add.patterns.find('.block_full_red').clone();
			clone_obj.appendTo(module_add.container);
			module_add.container.find('.block_full_red').last().find('input').focus();
			
			return false;
		});
		
		//add_double_block_blue_green init:
		$(".add_double_block_blue_green").click(function(){
			var clone_obj = module_add.patterns.find('.double_block_blue_green').clone();
			clone_obj.appendTo(module_add.container);
			module_add.container.find('.double_block_blue_green').last().find('textarea').eq(0).focus();
			
			module_add.container.find('.double_block_blue_green').last().find('textarea').keyup(function(){
				FitToContent( this, 500 )
			});
			
			return false;
		});
		
		
		//init dialog:
		module_add.image_dialog = $("#image_dialog").dialog({
			autoOpen: false,
			buttons: [
				{
				  text: "Выбрать",
				  click: function() {
					$( this ).dialog( "close" );
					
					var clone_obj = module_add.patterns.find('.block_image').clone();
					clone_obj.appendTo(module_add.container);
					module_add.container.find('.block_image').last().css("background-image", "url(" + module_add.image_dialog.find('input').val() + ")" );
					
				  }
				},
				{
					text: "Отмена",
					click: function(){
						$( this ).dialog( "close" );
					}
				}
			]
		});
		
		//show dialog on click:
		$(".add_block_image").click(function(){
			module_add.image_dialog.dialog("open");
			
			return false;
		});
	}
	
};

$(function(){
	module_add.init();
});


//fit to content textarea:
function FitToContent(id, maxHeight)
{
   var text = id && id.style ? id : document.getElementById(id);
   if ( !text )
      return;

   var adjustedHeight = text.clientHeight;
   if ( !maxHeight || maxHeight > adjustedHeight )
   {
      adjustedHeight = Math.max(text.scrollHeight, adjustedHeight);
      if ( maxHeight )
         adjustedHeight = Math.min(maxHeight, adjustedHeight);
      if ( adjustedHeight > text.clientHeight )
         text.style.height = adjustedHeight + "px";
   }
}