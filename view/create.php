<!-- left sidebar -->
<div class="left_sidebar">
	<b>Выберите действие:</b>
	<a href="#" class="add_block_full_red">Добавить блок с текстом на всю ширину (красный фон)</a>
	<a href="#" class="add_double_block_blue_green">Добавить двойной блок с текстом</a>
	<a href="#" class="add_block_image">Добавить картинку на всю ширину</a>
</div>
<!-- end left sidebar-->
<!-- preview container -->
<div class="right_perview_edit" id="perview_container">
	
</div>
<!-- end preview container-->

<div id="image_dialog" title="Выбор картинки" style="display: none;">
	<label>Укажите ссылку на картинку в следующее поле:</label>
	<input type="text" style="width: 100%;" />
</div>

<!-- block_patterns -->
<div id="block_patterns" class="hidden">
	<div class="block_full_red"><input type="text" /></div>
	<div class="double_block_blue_green">
		<div class="left_block block">
			<textarea></textarea>
		</div>
		<div class="right_block block">
			<textarea></textarea>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="block_image"></div>
</div>
<!-- end block patterns -->
