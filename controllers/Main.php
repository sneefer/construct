<?
	/**
	*	класс дефолтного контроллера, показывает стартовую страницу
	*
	*	
	*	@author Chavusov Stanislav
	*	@version 1.0
	*/
	class Main extends BaseController
	{
		/**
		*	обычный метод-обработчик главной страницы
		*
		*	
		*	@author Chavusov Stanislav
		*	@version 1.0
		*/
		public function index()
		{
			return $this->render('index',array());
		}
		
		/**
		*	запуск конструктора страницы
		*
		*	
		*	@author Chavusov Stanislav
		*	@version 1.0
		*/
		public function create()
		{
			$this->setLayout('add');
			return $this->render('create',array());
		}
		
	}