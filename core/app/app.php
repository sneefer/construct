<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . '../../modules/BaseController.php');
	/**
	*	класс приложения
	*
	*	инициализирует подгрузку настроек
	*	@author Chavusov Stanislav
	*	@version 1.0
	*/
	class App
	{
		protected $settings = null;
		protected $root_path = '';
		protected $template_dir = '';
		protected $default_layout = '';
		protected $web_path = '';
		protected $layout_vars = array();
		protected $host_name = '';
		
		/**
		*	сброка класса
		*	
		*	@version 1.0
		*/
		function __construct()
		{
			$this->loadSettings();
		}
		
		/**
		*	подгружает настройки
		*	
		*	@version 1.0
		*/
		protected function loadSettings()
		{
			$path = $_SERVER['DOCUMENT_ROOT'];
			
			$this->settings = parse_ini_file(substr($path,0,strlen($path)-3) . "/configs/system.ini", true);
			
			$this->setDefaultSettings();
		}
		
		/**
		*	выставляет настройки по-умолчанию из блока `default` по ключу:
		*	
		*	@version 1.0
		*/
		public function setDefaultSettings()
		{
			try
			{
				if (! array_key_exists("default",$this->settings))
				{
					throw new Exception('Блок `default` настроек не указан');
				}
				
				//set root_path and web_path:
				if (array_key_exists("root_path", $this->settings['default']))
				{
					$this->root_path = $this->settings['default']['root_path'];
					$this->web_path = $this->root_path . '/web';
				}
				else
				{
					throw new Exception('Настройка root_path секции `default` не найдена');
				}
				
				//set template_dir
				if (array_key_exists("template_dir", $this->settings['default']))
				{
					$this->template_dir = $this->settings['default']['template_dir'];
				}
				else
				{
					throw new Exception('Настройка root_path секции `default` не найдена');
				}
				
				//set default_layout
				if (array_key_exists("default_layout", $this->settings['default']))
				{
					$this->default_layout = $this->settings['default']['default_layout'];
				}
				else
				{
					throw new Exception('Настройка default_layout секции `default` не найдена');
				}
				
				//set host_name
				if (array_key_exists("host_name", $this->settings['default']))
				{
					$this->host_name = $this->settings['default']['host_name'];
				}
				else
				{
					throw new Exception('Настройка host_name секции `default` не найдена');
				}
			}
			catch(Exception $ex)
			{
				echo '<b style="color: #ff0000;">' . $ex->getMessage() . "</b>\n";
			}
		}
		
		/**
		*	получаем путь к папке с сайтом:
		*	
		*	@version 1.0
		*/
		public function getPath()
		{
			return $this->root_path;
		}
		
		/**
		*	получаем путь к папке с шаблонами конструктора:
		*	
		*	@version 1.0
		*/
		public function getPathToTemplates()
		{
			return $this->root_path . '/' . $this->template_dir;
		}
		
		/**
		*	формуруем обработку url-ов вида `q=controller/action`:
		*	
		*	@version 1.0
		*/
		public function getPage()
		{
			$controller = '';
			$action = '';
			
			if ($_GET['q'] != '')
			{
				$q = trim($_GET['q']);
				
				$urlp = explode("/",$q);
				if (count($urlp) == 2)
				{
					$controller = filter_var($urlp[0],FILTER_SANITIZE_STRING);
					$action = filter_var($urlp[1],FILTER_SANITIZE_STRING);
				}
				else
				{
					$controller = 'main';
					$action = 'index';
				}
			}
			else
			{
				//отдаём default page:
				$controller = 'main';
				$action = 'index';
			}
			
			if (file_exists($this->root_path . 'controllers/' . ucfirst($controller) . ".php"))
			{
				require_once($this->root_path . 'controllers/' . ucfirst($controller) . '.php');
				
				$className = ucfirst($controller);
				$classObj = new $className;
				$classObj->setApp($this);
				
				echo $classObj->$action();
			}
			else
			{
				echo '<b style="color: #ff0000;">Контроллер <i>' . $controller . "</i> не найден</b>\n";
			}
		}
		
		/**
		*	отдаём контент стандартной layout
		*	
		*	@version 1.0
		*/
		public function getDefaultLayoutContent()
		{
			$layout_content = '';

			if (file_exists($this->root_path . 'layout/' . $this->default_layout . '.php'))
			{
				$layout_content = file_get_contents($this->root_path . 'layout/' . $this->default_layout . '.php');
				
				//рендерим переменные для layout:
				foreach($this->layout_vars as $key=>$val)
				{
					$layout_content = str_replace("[#" . $key . "#]", $val, $layout_content);
				}
				
				//рендерим системные переменные:
				$layout_content = str_replace("[#host_name#]", $this->host_name, $layout_content);
			}
			return $layout_content;
		}
		
		/**
		*	замена стандартной layout
		*	
		*	@version 1.0
		*/
		public function replaceLayout($name)
		{
			$this->default_layout = $name;
		}
	}