<!DOCTYPE html>
<html>
<head>
	<title>Default title</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="[#host_name#]css/jquery-ui.min.css">
	<link rel="stylesheet" href="[#host_name#]css/style_add.css">
	<link rel="stylesheet" href="[#host_name#]bootstrap/css/bootstrap.min.css">
	<base href="[#host_name#]" />
	
</head>
<body>
	[#content#]
	
	<script type="text/javascript" src="[#host_name#]js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="[#host_name#]js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="[#host_name#]js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="[#host_name#]js/jquery.form.js"></script>
	<script type="text/javascript" src="[#host_name#]js/add.js"></script>
	<script type="text/javascript" src="[#host_name#]bootstrap/js/bootstrap.min.js"></script>
</body>
</html>