<?php
	require_once("../core/app/app.php");
	
	/**
	*	класс базового контроллера, формирует методы для остальных
	*
	*	
	*	@author Chavusov Stanislav
	*	@version 1.0
	*/
	class BaseController
	{
		private $app = null;
		
		/**
		*	получает app объект
		*
		*	
		*	@author Chavusov Stanislav
		*	@version 1.0
		*/
		public function setApp($appObj)
		{
			$this->app = $appObj;
			//echo 'set app';
		}
		
		/**
		*	устанавливает другой layout для последующего рендеринга
		*
		*	
		*	@author Chavusov Stanislav
		*	@version 1.0
		*/
		public function setLayout($name)
		{
			//print_r($this->app);
			$this->app->replaceLayout($name);
		}
		
		/**
		*	стандартный метод рендеринга страницы
		*
		*	
		*	@author Chavusov Stanislav
		*	@version 1.0
		*/
		public function render($view, $data)
		{
			
			$file_content = '';
			$layout_content = '';
			
			if (file_exists('../view/'.$view . '.php'))
			{
				$file_content = file_get_contents('../view/'.$view . '.php');
				
				foreach($data as $key=>$val)
				{
					$file_content = str_replace("[#" . $key . "#]", $val, $file_content);
				}
				
				$layout_content = $this->app->getDefaultLayoutContent();
				$layout_content = str_replace("[#content#]", $file_content, $layout_content);
				
			}
			
			return $layout_content;
		}
	}